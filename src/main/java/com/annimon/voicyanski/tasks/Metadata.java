package com.annimon.voicyanski.tasks;

import java.io.File;

public final class Metadata {

    public File file;
    public String artist;
    public String title;

    public boolean hasTrackInfo() {
        return (artist != null) || (title != null);
    }

    @Override
    public String toString() {
        if (!hasTrackInfo()) {
            return file.getName();
        }
        return String.format("%s - %s",
                (artist != null) ? artist : "Unknown",
                (title != null) ? title : "Unknown");
    }
}
