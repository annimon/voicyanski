package com.annimon.voicyanski.tasks;

import com.annimon.voicyanski.exceptions.FetchMetadataException;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class FetchMetadataTask implements Function<File, Metadata> {

    private static final int PROCESS_WAIT_TIME = 20;
    private static final Pattern ARTIST_PATTERN = Pattern.compile("^artist=(.*)$");
    private static final Pattern TITLE_PATTERN = Pattern.compile("^title=(.*)$");

    @Override
    public Metadata apply(File file) {
        final Metadata metadata = new Metadata();
        metadata.file = file;
        try {
            final ProcessBuilder pb = new ProcessBuilder(
                    "ffmpeg",
                    "-v", "error",
                    "-i", file.getAbsolutePath(),
                    "-f", "ffmetadata",
                    "-"
            );
            final Process ffmpeg = pb.start();
            final Scanner out = new Scanner(ffmpeg.getInputStream(), StandardCharsets.UTF_8.name());
            while (out.hasNextLine()) {
                final String line = out.nextLine();
                Matcher m;
                m = ARTIST_PATTERN.matcher(line);
                if (m.find()) {
                    metadata.artist = m.group(1);
                    continue;
                }
                m = TITLE_PATTERN.matcher(line);
                if (m.find()) {
                    metadata.title = m.group(1);
                }
            }
            ffmpeg.waitFor(PROCESS_WAIT_TIME, TimeUnit.SECONDS);
        } catch (InterruptedException | IOException ex) {
            throw new FetchMetadataException();
        }
        return metadata;
    }
}
