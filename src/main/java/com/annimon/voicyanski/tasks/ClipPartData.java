package com.annimon.voicyanski.tasks;

import java.io.File;

public final class ClipPartData {
    public Metadata metadata;
    public File outputAudioFile;
    public double startTime, endTime;
}
