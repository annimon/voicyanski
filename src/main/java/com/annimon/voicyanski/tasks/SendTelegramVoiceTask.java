package com.annimon.voicyanski.tasks;

import com.annimon.voicyanski.Main;
import com.annimon.voicyanski.exceptions.TelegramSendingException;
import java.io.IOException;
import java.util.function.Function;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public final class SendTelegramVoiceTask implements Function<ClipPartData, Boolean> {

    private static final String URL_FORMAT = "https://api.telegram.org/bot%s/%s";
    private static final MediaType MEDIA_TYPE_OGG = MediaType.parse("audio/ogg");

    private final OkHttpClient client = new OkHttpClient();

    @Override
    public Boolean apply(ClipPartData data) {
        final MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        if (data.metadata.hasTrackInfo()) {
            builder.addFormDataPart("caption", data.metadata.toString());
        }
        builder.addFormDataPart("chat_id", Main.getConfig().getTelegramChatId());
        builder.addFormDataPart("voice", data.metadata.file.getName(),
                RequestBody.create(MEDIA_TYPE_OGG, data.outputAudioFile));
        final RequestBody requestBody = builder.build();
        
        final Request request = new Request.Builder()
                .url(String.format(URL_FORMAT, Main.getConfig().getTelegramBotToken(), "sendVoice"))
                .post(requestBody)
                .build();

        try (final Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                throw new IOException("Unexpected code " + response);
            }
            return true;
        } catch (IOException ex) {
            throw new TelegramSendingException(ex);
        }
    }
}
