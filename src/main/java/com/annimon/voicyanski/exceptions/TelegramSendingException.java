package com.annimon.voicyanski.exceptions;

import com.annimon.voicyanski.Main;

public class TelegramSendingException extends RuntimeException {

    public TelegramSendingException(Throwable cause) {
        super("Unable to send message", cause);
    }

    @Override
    public String getLocalizedMessage() {
        return Main.getResources().getString("unable_send_message");
    }
}
