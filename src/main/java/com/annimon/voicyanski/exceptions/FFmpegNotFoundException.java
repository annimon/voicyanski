package com.annimon.voicyanski.exceptions;

import com.annimon.voicyanski.Main;

public class FFmpegNotFoundException extends RuntimeException {

    public FFmpegNotFoundException() {
        super("Unable to access FFmpeg. Please install it or add to PATH environment variable");
    }

    @Override
    public String getLocalizedMessage() {
        return Main.getResources().getString("unable_access_ffmpeg");
    }
}
