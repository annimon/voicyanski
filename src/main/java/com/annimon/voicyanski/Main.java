package com.annimon.voicyanski;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    private static ResourceBundle resources;
    private static Config config;

    public static ResourceBundle getResources() {
        return resources;
    }

    public static Config getConfig() {
        return config;
    }

    @Override
    public void start(Stage stage) throws Exception {
        final Path configPath = Paths.get(System.getProperty("user.home"), ".config", "voicyanski.conf");
        Main.config = new ConfigLoader("app", configPath).load();
        if (config.getLocale() != null) {
            Locale.setDefault(new Locale(config.getLocale()));
        }
        Main.resources = ResourceBundle.getBundle("locales.Language", Locale.getDefault());
        final FXMLLoader loader = new FXMLLoader(
                getClass().getResource("/fxml/Main.fxml"), Main.resources);

        Scene scene = new Scene(loader.load());
        scene.getStylesheets().add("/styles/main.css");

        MainController controller = (MainController) loader.getController();
        controller.setParameters(getParameters());
        controller.setPrimaryStage(stage);

        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
