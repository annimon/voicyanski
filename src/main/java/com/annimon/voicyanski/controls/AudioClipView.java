package com.annimon.voicyanski.controls;

import com.annimon.voicyanski.Main;
import com.annimon.voicyanski.audio.Waveform;
import com.annimon.voicyanski.tasks.Metadata;
import java.io.IOException;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.binding.ObjectBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;
import org.controlsfx.control.RangeSlider;
import org.kordamp.ikonli.Ikon;
import org.kordamp.ikonli.javafx.FontIcon;
import org.kordamp.ikonli.materialdesign.MaterialDesign;

public final class AudioClipView extends VBox {

    @FXML
    private Label lblInfo;

    @FXML
    private RangeSlider rangeClip;

    @FXML
    private WaveformView waveform;

    private final ObjectProperty<Metadata> metadataProperty = new SimpleObjectProperty<>();
    private final ObjectProperty<String> durationProperty = new SimpleObjectProperty<>();
    private MediaPlayer mediaPlayer;
    private FontIcon iconPlay;

    public AudioClipView() {
        final FXMLLoader fxmlLoader = new FXMLLoader(
                getClass().getResource("/fxml/controls/AudioClipView.fxml"));
        fxmlLoader.setRoot(AudioClipView.this);
        fxmlLoader.setController(AudioClipView.this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        initialize();
    }

    public Metadata getMetadata() {
        return metadataProperty.get();
    }

    public void setMetadata(Metadata value) {
        metadataProperty.set(value);
    }

    public ObjectProperty<Metadata> metadataProperty() {
        return metadataProperty;
    }

    public ObjectProperty<String> durationProperty() {
        return durationProperty;
    }

    public Waveform getWaveform() {
        return waveform.getWaveform();
    }

    public void setWaveform(Waveform waveform) {
        this.waveform.setWaveform(waveform);
    }

    public ObjectProperty<Waveform> waveformProperty() {
        return waveform.waveformProperty();
    }

    public double getClipPartStart() {
        return rangeClip.getLowValue();
    }

    public double getClipPartEnd() {
        return rangeClip.getHighValue();
    }

    public void setIconPlay(FontIcon iconPlay) {
        this.iconPlay = iconPlay;
    }

    public void reset() {
        setMetadata(null);
        setWaveform(null);
        stop();
    }

    public void playFrom(Duration d) {
        if (mediaPlayer == null) {
            return;
        }
        mediaPlayer.seek(d);
        mediaPlayer.play();
    }

    public void stop() {
        if (mediaPlayer == null) {
            return;
        }
        mediaPlayer.stop();
    }

    public void onLoad(Waveform wf) {
        setWaveform(wf);
        if (wf != null) {
            rangeClip.setLowValue(0);
            rangeClip.setMax(wf.getMaxTime());
            rangeClip.setHighValue(wf.getMaxTime());

            final Media media = new Media(getMetadata().file.toURI().toString());
            mediaPlayer = new MediaPlayer(media);
            mediaPlayer.startTimeProperty().bind(new ObjectBinding<Duration>() {
                {
                    bind(rangeClip.lowValueProperty());
                }
                @Override
                protected Duration computeValue() {
                    return Duration.seconds(rangeClip.getLowValue());
                }
            });
            mediaPlayer.stopTimeProperty().bind(new ObjectBinding<Duration>() {
                {
                    bind(rangeClip.highValueProperty());
                }
                @Override
                protected Duration computeValue() {
                    return Duration.seconds(rangeClip.getHighValue());
                }
            });
            waveform.currentPlayTimeProperty().bind(Bindings
                    .when(mediaPlayer.statusProperty().isEqualTo(MediaPlayer.Status.PLAYING)
                            .or(mediaPlayer.statusProperty().isEqualTo(MediaPlayer.Status.PAUSED)) )
                    .then(mediaPlayer.currentTimeProperty())
                    .otherwise(Duration.INDEFINITE)
            );
            waveform.setCustomCurrentTimeListener((observable, oldValue, newValue) -> {
                playFrom(newValue);
            });
            mediaPlayer.statusProperty().addListener((observable, oldValue, newValue) -> {
                final Ikon ikon = (newValue == MediaPlayer.Status.PLAYING)
                        ? MaterialDesign.MDI_PAUSE
                        : MaterialDesign.MDI_PLAY;
                iconPlay.setIconCode(ikon);
            });
        }
    }

    private void initialize() {
        rangeClip.managedProperty().bind(rangeClip.visibleProperty());
        rangeClip.visibleProperty().bind(waveformProperty().isNotNull());
        waveform.startProperty().bind(rangeClip.lowValueProperty());
        waveform.endProperty().bind(rangeClip.highValueProperty());

        lblInfo.managedProperty().bind(lblInfo.visibleProperty());
        lblInfo.visibleProperty().bind(waveformProperty().isNull());
        lblInfo.textProperty().bind(Bindings
                .when(metadataProperty.isNull())
                .then(Main.getResources().getString("drop_your_music_here"))
                .otherwise(Main.getResources().getString("processing_file"))
        );

        waveform.visibleProperty().bind(waveformProperty().isNotNull());

        durationProperty.bind(Bindings.createStringBinding(() -> {
            double hi = rangeClip.highValueProperty().get();
            double lo = rangeClip.lowValueProperty().get();
            final int delta = (int) (hi - lo);
            return String.format("%02d:%02d", (delta % 3600) / 60, delta % 60);
        }, rangeClip.highValueProperty(), rangeClip.lowValueProperty()));
    }

    public void initMarkButtons(Button markStart, Button markEnd) {
        markStart.setOnAction(event -> {
            if (mediaPlayer == null) return;
            final double time = mediaPlayer.currentTimeProperty().get().toSeconds();
            if (time < getClipPartEnd()) {
                rangeClip.setLowValue(time);
            }
        });
        markEnd.setOnAction(event -> {
            if (mediaPlayer == null) return;
            final double time = mediaPlayer.currentTimeProperty().get().toSeconds();
            if (time > getClipPartStart()) {
                rangeClip.setHighValue(time);
            }
        });
    }

    public void initPlayButtons(Button backward, Button playPause, Button forward) {
        backward.setOnAction(event -> playFrom(Duration.seconds(rangeClip.getLowValue())));
        forward.setOnAction(event -> {
            if (mediaPlayer == null) return;
            final double endTime = rangeClip.getHighValue();
            final double fromTime = Math.max(0, endTime - 3);
            playFrom(Duration.seconds(fromTime));
        });
        playPause.setOnAction(event -> {
            if (mediaPlayer == null) return;
            if (mediaPlayer.getStatus() == MediaPlayer.Status.PLAYING) {
                mediaPlayer.pause();
            } else {
                playFrom(mediaPlayer.getCurrentTime());
            }
        });
    }
}
