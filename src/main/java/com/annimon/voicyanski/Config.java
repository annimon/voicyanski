package com.annimon.voicyanski;

public class Config {

    private String locale;
    private String telegramChatId, telegramBotToken;
    private boolean encodeStereo;
    private String encodeBitrate;
    private String encodeSampleRate;

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getTelegramChatId() {
        return telegramChatId;
    }

    public void setTelegramChatId(String telegramChatId) {
        this.telegramChatId = telegramChatId;
    }

    public String getTelegramBotToken() {
        return telegramBotToken;
    }

    public void setTelegramBotToken(String telegramBotToken) {
        this.telegramBotToken = telegramBotToken;
    }

    public boolean isEncodeStereo() {
        return encodeStereo;
    }

    public void setEncodeStereo(boolean encodeStereo) {
        this.encodeStereo = encodeStereo;
    }

    public String getEncodeBitrate() {
        return encodeBitrate;
    }

    public void setEncodeBitrate(String encodeBitrate) {
        this.encodeBitrate = encodeBitrate;
    }

    public String getEncodeSampleRate() {
        return encodeSampleRate;
    }

    public void setEncodeSampleRate(String encodeSampleRate) {
        this.encodeSampleRate = encodeSampleRate;
    }
}
