package com.annimon.voicyanski;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;
import java.util.ResourceBundle;

public class ConfigLoader {

    private final String resourceName;
    private final Path externalPath;

    public ConfigLoader(String resourceName, Path externalPath) {
        this.resourceName = resourceName;
        this.externalPath = externalPath;
    }

    public Config load() {
        final Config config = new Config();
        final ResourceBundle res = ResourceBundle.getBundle(resourceName);
        try (InputStream extConfig = Files.newInputStream(externalPath)) {
            final Properties props = new Properties();
            props.load(extConfig);
            loadConfig(config, key -> props.getProperty(key, res.getString(key)));
        } catch (IOException ex) {
            loadConfig(config, res::getString);
        }
        return config;
    }

    private void loadConfig(Config config, PropertyLoader loader) {
        config.setLocale(loader.load("locale"));
        config.setTelegramChatId(loader.load("telegram-chat-id"));
        config.setTelegramBotToken(loader.load("telegram-bot-token"));
        config.setEncodeStereo(loader.load("encode-stereo").equalsIgnoreCase("true"));
        config.setEncodeBitrate(loader.load("encode-bitrate"));
        config.setEncodeSampleRate(loader.load("encode-samplerate"));
    }

    @FunctionalInterface
    private interface PropertyLoader {
        String load(String key);
    }
}
