package com.annimon.voicyanski.audio;

import com.annimon.voicyanski.exceptions.WaveformBuildException;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class WaveformFileReader implements WaveformReader {

    private final Path filePath;

    public WaveformFileReader(Path filePath) {
        this.filePath = filePath;
    }

    @Override
    public Waveform read() {
        final Waveform waveform = new Waveform();
        try (BufferedReader br = Files.newBufferedReader(filePath)) {
            final Pattern pattern = Pattern.compile("frame:(\\d+).*pts_time:([\\d.]+).*RMS_level=(-?[\\d.]+)");
            while (true) {
                String line1 = br.readLine();
                if (line1 == null) break;
                String line2 = br.readLine();
                if (line2 == null) break;

                Matcher m = pattern.matcher(line1 + line2);
                if (m.find()) {
                    int frame = Integer.parseInt(m.group(1), 10);
                    double time = Double.parseDouble(m.group(2));
                    double level = Double.parseDouble(m.group(3));
                    level = Math.signum(level) * Math.log(Math.abs(level));
                    waveform.addRms(new Rms(frame, time, level));
                }
            }
        } catch (IOException ex) {
            throw new WaveformBuildException(ex);
        }
        return waveform;
    }
}
