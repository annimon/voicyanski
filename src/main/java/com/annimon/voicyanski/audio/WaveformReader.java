package com.annimon.voicyanski.audio;

public interface WaveformReader {

    Waveform read();
}
